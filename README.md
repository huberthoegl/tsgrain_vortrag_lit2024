# tsgrain_vortrag_lit2024

- Install Quarto from <https://quarto.org>. I used `quarto-1.4.553-linux-amd64.tar.gz`.

- Run `make` to generate the `index.html` File. 

- Look at the talk here: <https://hhoegl.de/etc/tsgrain_vortrag_lit2024>

  You can use the following keys:

  - SPACE : next slide
  - p : previous slide
  - m : toggle menu
