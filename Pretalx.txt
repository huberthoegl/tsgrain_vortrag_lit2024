20. April 2024

https://pretalx.luga.de/lit-2024/me/submissions/FVFXKZ/

Titel

Embedded Linux am Beispiel der "Tsgrain" Beregnungssteuerung

Zusammenfassung

Der Vortrag gibt einen Abriss über die Disziplin "Embedded Linux" an Hand der
selbstentwickelten "Tsgrain" Beregnungssteuerung für Tennisplätze. Soft- und
Hardware sind frei so dass das Gerät jederzeit nachgebaut (und modifiziert)
werden kann.

Beschreibung

Durch den Ausfall einer aus den 80er Jahren stammenden Steuerung für
Sprinkleranlagen (mit 8085 Mikroprozessor) kam bei mir 2019 die Überlegung
auf, einen modernen Nachfolger  mit einem  preiswerten Community-Board wie z.B.
Raspberry Pi oder BeagleBone  zu bauen, der unter GNU/Linux läuft. Das Vorhaben
entpuppte sich als fast ideale Spielwiese für die Disziplin "Embedded Linux".
Seit 2020 ist der Prototyp im Einsatz, in den letzten Jahren sind immer wieder
Verbesserungen gemacht worden. 

Es wird um folgende Themen gehen:

- Auswahl geeigneter Hardware und der Linux Distribution
- die Ansteuerung von Peripherie wie I2C Port Expander, Taster, LEDs, Relais
- Störungen durch induktive Verbraucher und ihre Verhinderung
- Aufteilung der Steuerungs-Software in verschiedene Server für den Controller und die Web-Bedienung
  (Python)
- Kommunikation über gRPC
- Strategien für den Update der Software
- Betrieb in einer Umgebung ohne Internet-Verbindung (aber mit LoRa Anbindung)

Weiterführende Links

- https://www.tha.de/Informatik/Moderne-Beregnungssteuerung-fuer-Tennisanlagen.html
- https://hhoegl.de/tsgrain/index.html
- Vortragsfolien: https://hhoegl.de/etc/tsgrain_vortrag_lit2024



